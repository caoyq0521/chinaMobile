$(function(){
    $(".head-nav li").click(function(){
        var index=$(this).index();
        $(".head-nav li").removeClass("active");
        $(this).addClass("active");
        $(".search-mes").css("display","none");
        $(".search-mes").eq(index).css("display","block");
    });
    $(".add_vendor span").click(function(){
        var index=$(this).index();
        $(".add_vendor span").removeClass("vendor_active");
        $(this).addClass("vendor_active");
        $(".vendor_mes").css("display","none");
        $(".vendor_mes").eq(index).css("display","block");
    });
});